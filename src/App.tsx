import './App.css'
import Integration from './components/Integration'
import YoutubeForm from './components/YoutubeForm'
import { YupYouTubeForm } from './components/YupYouTubeForm'
import { ZodYouTubeForm } from './components/ZodYouTubeForm'

function App () {
  return (
    <div className='app'>
      <h2>Youtube Form </h2>
      {/* <YoutubeForm /> */}
      {/* <Integration /> */}
      {/* <YupYouTubeForm /> */}
      <ZodYouTubeForm />
    </div>
  )
}

export default App
