import { FieldErrors, useFieldArray, useForm } from 'react-hook-form'
import { DevTool } from '@hookform/devtools'
import { useEffect } from 'react'

let renderCount = 0

type FormValues = {
  username: string
  email: string
  channel: string
  social: {
    twitter: string
    facebook: string
  }
  phoneNumber: string[]
  phNumber: { number: string }[]
  age: number
  dob: Date
}

function YoutubeForm () {
  const form = useForm<FormValues>({
    defaultValues: {
      username: 'Batman',
      email: '',
      channel: '',
      social: {
        twitter: '',
        facebook: ''
      },
      phoneNumber: ['', ''],
      phNumber: [{ number: '' }],
      age: 0,
      dob: new Date()
    },
    mode: 'onSubmit'
    // mode: 'onBlur'
    // mode:'onTouched'
    // mode: 'onSubmit'

    // defaultValues: async () => {
    //     const response = await fetch(
    //       'https://jsonplaceholder.typicode.com/users/2'
    //     )
    //     const data = await response.json()
    //     return {
    //       username: 'Batman',
    //       email: data.email,
    //       channel: ''
    //     }
    //   }
  })
  //   console.log(form)
  const {
    register,
    control,
    handleSubmit,
    formState,
    watch,
    getValues,
    setValue,
    reset,
    trigger
  } = form
  //   console.log(register('username'))
  //   const { name, ref, onChange, onBlur } = register('username')
  renderCount++
  const { fields, append, remove } = useFieldArray({
    name: 'phNumber',
    control
  })

  const {
    errors,
    touchedFields,
    dirtyFields,
    isDirty,
    isValid,
    isSubmitting,
    isSubmitSuccessful,
    isSubmitted
  } = formState

  //   console.log({ isSubmitting }, { isSubmitSuccessful }, { isSubmitted })

  //   console.log(touchedFields, dirtyFields, isDirty)
  //   const watchUsername = watch('username')

  const onSubmit = (data: FormValues) => {
    console.log('form submitted', data)
  }

  //   useEffect(() => {
  //     const subscription = watch(value => {
  //       console.log(value)
  //     })
  //     return () => subscription.unsubscribe()
  //   }, [watch])

  useEffect(() => {
    if (isSubmitSuccessful) {
      reset()
    }
  }, [isSubmitSuccessful, reset])

  const handleGetValues = () => {
    // console.log('get Values', getValues(['social', 'age']))
    console.log('get Values', getValues())
  }

  const handleSetValue = () => {
    setValue('username', '', {
      shouldDirty: true,
      shouldTouch: true,
      shouldValidate: true
    })
  }

  const onError = (errors: FieldErrors<FormValues>) => {
    console.log('form errors', errors)
  }

  return (
    <>
      <h2>renderCount : {renderCount / 2}</h2>
      {/* <h2>watched value: {watchUsername}</h2> */}

      <div className='formpage'>
        <form onSubmit={handleSubmit(onSubmit, onError)} noValidate>
          <label htmlFor='username'>Username</label>
          <input
            type='text'
            {...register('username', { required: 'userName is required' })}
          />
          <p>{errors.username?.message}</p>

          <label>Email</label>
          <input
            type='email'
            id='email'
            {...register('email', {
              required: 'Email is required',
              pattern: {
                value: /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/i,
                message: 'Invalid email address'
              },
              validate: {
                notAdmin: fieldValue => {
                  return (
                    fieldValue !== 'admin@example.com' || 'Enter diffent email'
                  )
                },
                notBlackListed: fieldValue => {
                  return (
                    !fieldValue.endsWith('baddomain.com') ||
                    'This Domain is not allowed'
                  )
                },
                emailAvailable: async fieldValue => {
                  // console.log(fieldValue)
                  const response = await fetch(
                    `https://jsonplaceholder.typicode.com/users?email=${fieldValue}`
                  )
                  const data = await response.json()
                  return data.length === 0 || 'Email already exits'
                }
              }
            })}
          />
          <p>{errors.email?.message}</p>
          <label htmlFor='channel'>Channel</label>
          <input type='text' {...register('channel')} />
          <p>{errors.channel?.message}</p>

          <label htmlFor='facebook'>facebook</label>
          <input type='text' id='facebook' {...register('social.facebook')} />
          <p>{errors.social?.facebook?.message}</p>
          <label htmlFor='twitter'>twitter</label>
          <input
            type='text'
            id='facebook'
            {...register('social.twitter', {
              disabled: watch('channel') === ''
            })}
          />
          <p>{errors.social?.twitter?.message}</p>

          <label htmlFor='phoneNumber'>phoneNumber</label>
          <input
            type='text'
            id='phoneNumber1'
            {...register('phoneNumber.0', {
              required: 'primary number needed'
            })}
          />
          <p>{errors?.phoneNumber?.[0]?.message}</p>

          <label htmlFor='phoneNumber2'>phoneNumber 2</label>
          <input type='text' id='phoneNumber2' {...register('phoneNumber.1')} />

          <div>
            <label>list of Phone Numbers</label>
            <div>
              {fields.map((field, index) => {
                return (
                  <div key={field.id} className='form-control'>
                    <input
                      type='text'
                      {...register(`phNumber.${index}.number` as const)}
                    />
                    {index > 0 && (
                      <button type='button' onClick={() => remove(index)}>
                        remove
                      </button>
                    )}
                  </div>
                )
              })}
              <button type='button' onClick={() => append({ number: '' })}>
                Add PhoneNumber
              </button>
            </div>
          </div>

          <label htmlFor=''>Age</label>
          <input type='number' {...register('age', { valueAsNumber: true })} />

          <label htmlFor=''>Dob</label>
          <input type='date' {...register('dob', { valueAsDate: true })} />

          <button type='button' onClick={handleGetValues}>
            get Values
          </button>
          <button type='button' onClick={handleSetValue}>
            set Value
          </button>
          <button type='button' onClick={() => trigger('email')}>
            Validate
          </button>

          {/* <button type='button' onClick={() => reset()}>
            reset
          </button> */}

          <button
          //   disabled={!isDirty || !isValid}
          >
            Submit
          </button>
        </form>
        <DevTool control={control} />
      </div>
    </>
  )
}

export default YoutubeForm
