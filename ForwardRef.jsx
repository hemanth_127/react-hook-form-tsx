import React, { forwardRef, useRef } from 'react'

// Child component that will receive the ref
const ChildComponent = forwardRef((props, ref) => {
  // Access the DOM node or React component instance using the ref
  return <input ref={ref} />
})

// Parent component
const ParentComponent = () => {
  // Create a ref using useRef
  const inputRef = useRef(null)

  // Function to focus on the input element
  const focusInput = () => {
    inputRef.current.focus()
    console.log(inputRef.current.value)
  }

  return (
    <div>
      {/* Pass the ref to the ChildComponent */}
      <ChildComponent ref={inputRef} />
      <button onClick={focusInput}>Focus Input</button>
    </div>
  )
}

export default ParentComponent
